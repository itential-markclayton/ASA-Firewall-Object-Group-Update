<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
<!-- Update the below line with your artifact name -->
# ASA Firewall Object Group Update 
<!-- Add a short description or tag-line -->
This artifact contains the Cisco ASA group policy object update workflow for Ansible using IAP.  This workflow is designed to create and/or update an existing group object to add and remove hosts on a Cisco ASA firewall physical and virtual devices via Ansible.
​​
<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents
​
- [Overview](#overview)
- [Features](#features)
- [Requirements](#requirements)
- [Future Enhancements](#future-enhancements)
- [How to Install](#how-to-install)
- [How to Run](#how-to-run)
- [Additional Information](#additional-information)
​
## Overview
<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between their different IAP environments -->
<!--  (e.g. from Dev to Pre-Production or from Lab to Production).  -->
This artifact contains the Cisco ASA group policy object update workflow for Ansible using IAP.  This workflow is designed to create and/or update an existing group object to add and remove hosts on a Cisco ASA firewall physical and virtual devices via Ansible.
​
This solution consists of the following:
* Main Workflow (**IAP-Artifacts ASA Firewall Group Policy Update**)
* Automation Catalog entry (**IAP Artifacts ASA Firewall Object Group Update**)
* JSON Form (**IAP Artifacts ASA Firewall Object Group Update**)
​
<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GITLAB -->
<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update/raw/master/images/overview.png" alt="workflow" width="800px">
</td></tr></table>

​
## Features
<!-- Bullet point highlighting the most exciting features of the artifact -->
<!-- Ex.: * Automatically checks for device type -->
<!-- Ex.: * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- Ex.: * Verifies downloaded file integrity (using md5), will try to download again if failed -->
  * Group Update Host Addition and Removal.
  * Allow for a rollback in case functionality checks have failed.
  * Zero touch mode of operation is available to select via JSON form.
​
## Requirements
<!-- Explain any pre-req. Ex.: This artifact requires Ansible or NSO (with F5 NED) in order to run -->
This artifact requires Ansible, Itential Automation Gateway, and a Cisco ASA device.
​
## Supported Device Types
  * Cisco ASA/ASAv
​
## Test Environment
  * Devices Ver.:
    * asa: 9.12
  * IAP Ver. 2019.3.2
  * Itential Automation Gateway Ver.: 2.36.3+2019.3.14
  * Ansible Ver.: 2.8.3
​
## Future Enhancements
<!-- OPTIONAL Mention If the artifact has known limited support. Ex.: This artifact only support Cisco IOS and XR devices -->
* This workflow is designed to run only against Ansible devices; don't try to run it against Cisco-ASA devices orchastrated by Cisco NSO.
* Form validation on list of hosts only supports IPv4 addresses
​
## How to Install
<!-- OPTIONAL - Explain if external (to IAP) components are required. Ex. The ansible roles required for this artifact can be found in www.gitlab.com/itentialopensource......   -->
 * To install this artifact in IAP, use **App-Artifact** available in Nexus repo. 
 <table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update/raw/master/images/install.png" alt="workflow" width="800px">
</td></tr></table>
​

## How to Run
<!-- Explain what is/are the main entrypoint(s) for this artifact: automation catalog document, workflow builder, postman, etc.  -->
Starting the ASA Firewall Group Policy Update is done via the Automation Catalog. Please provide the device, group and hosts in the provided form.
 <table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update/raw/master/images/runAutomation.png" alt="workflow" width="800px">
</td></tr></table>

## Additional Information
Please use your Itential Customer Success account if you need support when using this artifact.
