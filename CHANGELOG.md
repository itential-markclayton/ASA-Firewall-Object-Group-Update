
## 0.0.7 [04-15-2020]

* Update artifact.json, package.json files

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!10

---

## 0.0.6 [04-13-2020]

* Remove app-artifacts from IAPDependencies in package.json and artifact.json

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!9

---

## 0.0.5 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!6

---

## 0.0.4 [03-24-2020]

* update README

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!5

---

## 0.0.3 [03-24-2020]

* merge release/2019.3 README onto master

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!3

---

## 0.0.2 [03-24-2020]

* merge release/2019.3 README onto master

See merge request itentialopensource/pre-built-automations/ASA-Firewall-Object-Group-Update!3

---
